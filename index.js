//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
let studentOneName = "John";
let studentOneEmail = "john@mail.com";
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = "Joe";
let studentTwoEmail = "joe@mail.com";
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = "Jane";
let studentThreeEmail = "jane@mail.com";
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = "Jessie";
let studentFourEmail = "jessie@mail.com";
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email) {
  console.log(`${email} has logged in`);
}

function logout(email) {
  console.log(`${email} has logged out`);
}

function listGrades(grades) {
  grades.forEach((grade) => {
    console.log(grade);
  });
}

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

//Use an object literal
//ENCAPSULATION

function studentObj(name, email, grades) {
  return {
    name: name,
    email: email,
    grades,
    grades,
    login() {
      console.log(`${this.email} has logged in`);
    },
    logout() {
      console.log(`${this.email} has logged out`);
    },
    listGrades() {
      console.log(
        `${this.name}'s quarterly grade averages are: ${this.grades}`
      );
    },
    averageGrade() {
      let ave = this.grades.reduce((x, y) => x + y, 0) / this.grades.length;
      // console.log(`The overall average for 4 quarters is ${ave}`);
      return ave;
    },
    willPass() {
      let eval = this.averageGrade() >= 75 ? true : false;
      // console.log(eval);
      return eval;
    },
    willPassWithHonors() {
      let eval = this.averageGrade() >= 90 ? true : false;
      // console.log(eval);
      return eval;
    },
  };
}

let studentOne = studentObj("John", "john@mail.com", [90, 95, 88, 98]);
//   name: "John",
//   email: "john@mail.com",
//   grades: [90, 95, 88, 98],

//   login() {
//     console.log(`${this.email} has logged in`);
//   },
//   logout() {
//     console.log(`${this.email} has logged out`);
//   },
//   listGrades() {
//     console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
//   },
//   averageGrade() {
//     let ave = this.grades.reduce((x, y) => x + y, 0) / this.grades.length;
//     console.log(`The overall average for 4 quarters is ${ave}`);
//     return ave;
//   },
//   willPass() {
//     this.averageGrade() >= 75 ? console.log(true) : console.log(false);
//   },
//   willPassWithHonors() {
//     this.averageGrade() >= 90 ? console.log(true) : console.log(false);
//   },
// };

console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quarterly grade average are ${studentOne.grades}`);

//[Activity Part 1]
/*
 1. Spaghetti code
 2. let object = {key:value} pairs
 3. OOP
 4. dot notation: studentOne.enroll()
 5. true
 6. {key:value}
 7. true
 8. true
 9. true
 10. true
*/

//[Activity Part 2]
console.log("[Item 1]");
let studentTwo = studentObj("Joe", "joe@mail.com", [88, 87, 89, 90]);
console.log(studentTwo);
let studentThree = studentObj("Jane", "jane@mail.com", [88, 89, 77, 78]);
console.log(studentThree);
let studentFour = studentObj("Jessie", "jessie@mail.com", [95, 96, 91, 85]);
console.log(studentTwo);

console.log("[Item 2]");
console.log(studentOne.averageGrade());
console.log(studentTwo.averageGrade());
console.log(studentThree.averageGrade());
console.log(studentFour.averageGrade());

console.log("[Item 3]");
console.log(studentOne.willPass());
console.log(studentTwo.willPass());
console.log(studentThree.willPass());
console.log(studentFour.willPass());

console.log("[Item 4]");
console.log(studentOne.willPassWithHonors());
console.log(studentTwo.willPassWithHonors());
console.log(studentThree.willPassWithHonors());
console.log(studentFour.willPassWithHonors());

console.log("[Item 5]");

let classOf1A = {
  students: [studentOne, studentTwo, studentThree, studentFour],
  countHonorStudents() {
    let countHonorStudent = 0;
    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        countHonorStudent += 1;
      }
    });
    return countHonorStudent;
  },
  honorPercentage() {
    let hP = this.countHonorStudents() / this.students.length;
    return `${(hP * 100).toFixed(0)}%`;
  },
  retrieveHonorStudentInfo() {
    let honorStudentInfo = [];
    this.students.forEach((student) => {
      if (student.willPassWithHonors()) {
        honorStudentInfo.push({
          aveGrade: student.averageGrade(),
          email: student.email,
        });
      }
    });
    return honorStudentInfo;
  },
  sortHonorStudentByGradeDesc() {
    let honorStudents = this.retrieveHonorStudentInfo();
    honorStudents.sort((s1, s2) =>
      s1.aveGrade > s2.aveGrade ? -1 : s1.aveGrade < s2.aveGrade ? 1 : 0
    );
    return honorStudents;
  },
};

console.log(classOf1A);

console.log("[Item 6]");
console.log(classOf1A.countHonorStudents());

console.log("[Item 7]");
console.log(classOf1A.honorPercentage());

console.log("[Item 8]");
console.log(classOf1A.retrieveHonorStudentInfo());

console.log("[Item 9]");
console.log(classOf1A.sortHonorStudentByGradeDesc());
